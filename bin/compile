#!/usr/bin/env bash

# indent output by eight spaces as recommended by the Heroku docs
indent() {
    sed -u 's/^/      /'
}

# script input
BUILD_DIR="$1"
CACHE_DIR="$2"
ENV_DIR="$3"
STAGE="$(mktemp -d)"

# this pack is valid for apps with an APP_ROOT environmental variable
if [ ! -f "${ENV_DIR}/APP_ROOT" ]; then
    echo "APP_ROOT was not set. Aborting" | indent
    exit 1
fi

# read values of APP_ROOT and APP_FOLDERS
APP_ROOT="$(cat "${ENV_DIR}/APP_ROOT")"
APP_FOLDERS=""

if [ -f "${ENV_DIR}/APP_FOLDERS" ]; then
    APP_FOLDERS="$(cat "${ENV_DIR}/APP_FOLDERS")"
fi

mv "${BUILD_DIR}/${APP_ROOT}"/* "${STAGE}"

if [ $? -ne 0 ]; then
    echo "FAILED to move ${APP_ROOT} into place" | indent
    exit 1
fi

for FOLDER in $(echo "${APP_FOLDERS}" | tr ";" "\n"); do
    mv "${BUILD_DIR}/${FOLDER}" "${STAGE}"

    if [ $? -ne 0 ]; then
        echo "FAILED to move ${FOLDER} into place" | indent
        exit 1
    fi
done

rm -rf "${BUILD_DIR}" && mkdir "${BUILD_DIR}" && mv "${STAGE}"/* "${BUILD_DIR}"

if [ $? -ne 0 ]; then
    echo "FAILED to copy directory into place" | indent
    exit 1
fi

echo "Copied ${APP_ROOT} to root and ${APP_FOLDERS} to folders of app successfully" | indent

